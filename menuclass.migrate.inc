<?php
/**
 * This constant determines the behavior of the migration process from Menu
 * Class to Menu Attributes in the case that a menu item already has a class
 * definition under Menu Attributes.
 *
 * To enable the migration process uncomment the define line and replace 'skip'
 * with one of the following
 *
 * @ 'skip': Leave as 'skip' (the default behavior) to skip menu items that
 *   already have a class in Menu Attributes (doesn't change existing classes)
 * @ 'override': Replace with 'override' to completely disregard the existing
 *   Menu Attributes class and write the one from Menu Class instead
 * @ 'append': Replace with 'append' to add the class from Menu Class to the
 *   class from Menu Attributes
 */

// define('MENUCLASS_MIGRATE_TO_MENU_ATTRIBUTES', 'skip');
