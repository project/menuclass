<?php

/**
 * @file Installer file for Menu Class module.
 */

/**
 * Implementation of hook_schema().
 */
function menuclass_schema() {
  $schema['menu_class_set'] = array(
    'description' => t('Sets of classes.'),
    'fields' => array(
      'csid' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => t('Primary key: Unique set ID.'),
      ),
      'title' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
        'descriptions' => t('Title of the classes set.'),
      ),
    ),
    'primary key' => array('csid'),
    'unique keys' => array(
      'title' => array('title'),
    ),
  );
  
  $schema['menu_class_definition'] = array(
    'description' => t('Classes names.'),
    'fields' => array(
      'classid' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => t('Primary Key: Unique class ID.'),
      ),
      'csid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t('Index: The set to which this class belong. Reference {menu_class_set}.csid.'),
      ),
      'weight' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
        'description' => t("The class's weight."),
      ),
      'definition' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
        'description' => t("The class name or names."),
      ),
    ),
    'primary key' => array('classid'),
    'indexes' => array(
      'csid' => array('csid'),
    ),
  );
  
  $schema['menu_links_class'] = array(
    'description' => t('Join table for menu items and their classes.'),
    'fields' => array(
      'mlid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t("Primary Key: Reference for {menu_links}.mlid."),
      ),
      'classid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t("Primary Key: Reference for {menu_class_definition}.classid."),
      ),
    ),
    'primary key' => array(
      'mlid',
      'classid',
    ),
  );
  
  return $schema;
}

/**
 * Implementation of hook_install().
 */
function menuclass_install() {
  // Create tables.
  drupal_install_schema('menuclass');
}

/**
 * Implementation of hook_uninstall().
 */
function menuclass_uninstall() {
  // Remove variables. Ugly, but I'm not sure there's another method.
  db_query("DELETE FROM {variable} WHERE name LIKE 'menuclass_%'");
  cache_clear_all('variables', 'cache');
  
  // Remove tables.
  drupal_uninstall_schema('menuclass');
}

/**
 * Implementation of hook_update_N().
 *
 * Please read MIGRATE.txt in the Menu Class's directory before continueing.
 */
function menuclass_update_6000() {
  $ret = array();
  
  if (!module_exists('menu_attributes')) {
    $ret['#abort'] = array(
      'success' => FALSE,
      'query' => t('Menu Attributes is not installed and enabled.'),
    );
    
    return $ret;
  }
  
  module_load_include('inc', 'menuclass', 'menuclass.migrate');
  if (!defined('MENUCLASS_MIGRATE_TO_MENU_ATTRIBUTES')) {
    $ret['#abort'] = array(
      'success' => FALSE,
      'query' => t("Please read MIGRATE.txt in the Menu Class's directory before continueing."),
    );
    
    return $ret;
  }
  
  $classes = array();
  $result = db_query('SELECT mlc.mlid, mcd.definition FROM {menu_class_definition} mcd, {menu_links_class} mlc WHERE mcd.classid = mlc.classid');
  while ($row = db_fetch_object($result)) {
    $classes[$row->mlid][] = $row->definition;
  }
  
  foreach ($classes as $mlid => $class) {
    $menu_link = menu_link_load($mlid);
    
    if (!empty($menu_link['options']['attributes']['class'])) {
      // Menu Attributes already has a class here
      switch (MENUCLASS_MIGRATE_TO_MENU_ATTRIBUTES) {
        case 'override':
          // Simply break out of the switch, will override the classes
          break;
          
        case 'append':
          // Append the class from Menu Attributes to the classes from Menu Class
          $class[] = $menu_link['options']['attributes']['class'];
          break;
          
        case 'skip':
        default:
          // Continue to the next menu item
          continue 2;
          break;
      }
      
    }
    
    $menu_link['options']['attributes']['class'] = trim(implode(' ', $class));
    menu_link_save($menu_link);
  }
  
  // Disable Menu Class. So long, self!
  module_disable(array('menuclass'));
  
  $ret['migrated'] = array(
    'success' => TRUE,
    'query' => t('Menu Class was migrated to Menu Attributes and is now disabled.'),
  );
  
  return $ret;
}
